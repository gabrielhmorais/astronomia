---
title: "Quasares"
date: 2021-10-23T19:25:59-03:00
draft: false
---

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Quasar, abreviação de quasi-stellar radio source ("fonte de rádio quase estelar") ou quasi-stellar object ("objeto quase estelar"), é um núcleo galáctico ativo, de tamanho maior que o de uma estrela, porém menor do que o mínimo para ser considerado uma galáxia.

&nbsp;

Diferentemente do que filmes como Star Wars sugerem, as batalhas entre naves espaciais não geram estrondosos sons de explosões. Aliás, não geram som nenhum porque o espaço é completamente silencioso. Essa é a realidade porque as ondas sonoras precisam de meios para se propagar. Como não há atmosfera no vácuo, o espaço é completamente silencioso. Por isso a ideia de se perder no espaço é tão aterrorizante, já que literalmente ninguém ouviria você gritar.

&nbsp;

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a massa dolor. Pellentesque sagittis mi in leo hendrerit, sed gravida augue vestibulum. Nunc sollicitudin suscipit risus quis lobortis. Vivamus pellentesque vulputate vulputate. Nunc a orci auctor, congue magna ac, facilisis sapien. Cras et nulla rhoncus, molestie arcu nec, dapibus purus. Integer ultricies nisi ac pulvinar maximus. Curabitur mauris arcu, bibendum id nisl vel, aliquet varius urna.

&nbsp;

In auctor volutpat lectus ac vulputate. Integer nisi justo, consectetur ac accumsan ut, hendrerit a nunc. Mauris eget mauris vitae erat posuere dapibus vel sed eros. Sed feugiat euismod dui ac mattis. Vivamus nec velit vitae purus condimentum commodo a sit amet tortor. Praesent libero odio, interdum convallis sem sit amet, commodo volutpat quam. Nulla facilisi.

&nbsp;

Etiam imperdiet sapien at velit tempus, et malesuada ligula molestie. Nulla eget mauris lacinia, fringilla metus eu, scelerisque mauris. Integer quis nisi risus. Etiam libero tortor, cursus quis tellus eu, elementum tristique felis. Nunc a ipsum justo. Cras volutpat neque in augue eleifend sollicitudin. Pellentesque interdum mollis nunc, ut auctor sem interdum id.

&nbsp;

Aliquam eget metus sagittis odio pretium efficitur. Sed ullamcorper consectetur ante, a luctus nisl elementum sed. Nullam rutrum gravida gravida. Phasellus facilisis bibendum ipsum eget pharetra. Donec sollicitudin neque a nibh rhoncus, non laoreet ligula convallis. Maecenas ac luctus arcu. Nam laoreet neque justo, hendrerit sagittis ex posuere id. Nulla vitae pulvinar odio. Suspendisse potenti. Cras eros enim, feugiat id dui eget, suscipit porta augue. Vivamus vitae mauris mi. Praesent aliquam, enim ut tincidunt porttitor, odio quam molestie turpis, non fermentum ex elit sed ante. Mauris gravida, magna et sagittis laoreet, augue tellus maximus quam, a congue augue odio vel ipsum.

&nbsp;

Vestibulum bibendum et eros eget congue. In velit tellus, venenatis et vulputate id, cursus ac justo. Suspendisse quis suscipit arcu. Mauris facilisis consectetur tristique. Ut lobortis mi sit amet faucibus vulputate. Nunc semper viverra nibh, eget accumsan velit. Sed pretium mauris et magna viverra rhoncus. Sed luctus odio sit amet ullamcorper molestie. Praesent risus lacus, cursus at nisl et, rhoncus efficitur magna. Sed porta nunc in augue laoreet dapibus. Vivamus pharetra vestibulum odio, laoreet tempus libero eleifend at. Curabitur euismod dui et metus laoreet, ac aliquet orci porttitor. Sed non auctor ante, ac euismod lectus. Fusce ante nisl, volutpat eget pellentesque commodo, scelerisque fringilla nibh. Pellentesque id porta lectus, et lacinia nibh. Suspendisse potenti.